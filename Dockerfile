FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN gpg --keyserver keyserver.ubuntu.com --recv FF054DACD908493A

# just a mirror of https://kolaente.dev/vikunja/vikunja
# renovate: datasource=github-tags depName=go-vikunja/vikunja versioning=semver extractVersion=^v(?<version>.+)$
ARG VIKUNJA_VERSION=0.24.6

RUN wget https://dl.vikunja.io/vikunja/${VIKUNJA_VERSION}/vikunja-v${VIKUNJA_VERSION}-linux-amd64-full.zip -O /tmp/vikunja.zip && \
    unzip /tmp/vikunja.zip -d /app/code && \
    wget https://dl.vikunja.io/vikunja/${VIKUNJA_VERSION}/vikunja-v${VIKUNJA_VERSION}-linux-amd64-full.zip.asc -O /tmp/vikunja.asc && \
    gpg --verify /tmp/vikunja.asc /tmp/vikunja.zip && \
    mv /app/code/vikunja-v${VIKUNJA_VERSION}-linux-amd64 /usr/bin/vikunja && \
    rm /app/code/*

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
