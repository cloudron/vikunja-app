#!/bin/bash

set -eu

mkdir -p /app/data/files \
        /app/data/logs \
        /run/vikunja

echo "=> Changing ownership"
chown -R cloudron:cloudron /app/data

# Creating config.yml and a secret APP_KEY if it does not exist
if [[ ! -f /app/data/config.yml ]]; then
  echo "==> Start with fresh config.yml"
  touch /app/data/config.yml

  APP_KEY="$(dd if=/dev/urandom bs=32 count=1 | base64)"
  cp /app/code/api/config.yml.sample /app/data/config.yml
  yq e ".service.JWTSecret=\"${APP_KEY}\"" -i /app/data/config.yml

  if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    yq e ".service.enableregistration=false" -i /app/data/config.yml
    yq e ".auth.local.enabled=false" -i /app/data/config.yml
  else
    yq e ".service.enableregistration=true" -i /app/data/config.yml
  fi
fi

# Replace config in config.yaml
echo "=> Ensure configs"

# Service config
yq e ".service.rootpath=\"/app/data/\"" -i /app/data/config.yml
yq e "del(.service.frontendurl)" -i /app/data/config.yml
yq e ".service.publicurl=\"${CLOUDRON_APP_ORIGIN}/\"" -i /app/data/config.yml

# Unset defaultsettings for new users
yq e ".defaultsettings.timezone=\"\"" -i /app/data/config.yml
yq e ".defaultsettings.language=\"\"" -i /app/data/config.yml

# Database config - use postgres
yq e ".database.type=\"postgres\"" -i /app/data/config.yml
yq e ".database.user=\"${CLOUDRON_POSTGRESQL_USERNAME}\"" -i /app/data/config.yml
yq e ".database.password=\"${CLOUDRON_POSTGRESQL_PASSWORD}\"" -i /app/data/config.yml
yq e ".database.host=\"${CLOUDRON_POSTGRESQL_HOST}\"" -i /app/data/config.yml
yq e ".database.database=\"${CLOUDRON_POSTGRESQL_DATABASE}\"" -i /app/data/config.yml

# Cache config (enabling cache causes all sorts of errors)
yq e ".cache.enabled=false" -i /app/data/config.yml

# Mail config - Can only be used encrypted
yq e ".mailer.enabled=true" -i /app/data/config.yml
yq e ".mailer.host=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" -i /app/data/config.yml
yq e ".mailer.port=\"${CLOUDRON_MAIL_STARTTLS_PORT}\"" -i /app/data/config.yml
yq e ".mailer.username=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" -i /app/data/config.yml
yq e ".mailer.password=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" -i /app/data/config.yml
yq e ".mailer.fromemail=\"${CLOUDRON_MAIL_FROM}\"" -i /app/data/config.yml
yq e ".mailer.skiptlsverify=true" -i /app/data/config.yml
yq e ".mailer.forcessl=false" -i /app/data/config.yml

# Log config
yq e ".log.path=\"/app/data/logs\"" -i /app/data/config.yml

# Files config
yq e ".files.basepath=\"/app/data/files/\"" -i /app/data/config.yml

# OIDC config
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
  yq e ".auth.openid.enabled=true" -i /app/data/config.yml
  yq e ".auth.openid.redirecturl=\"${CLOUDRON_APP_ORIGIN}/auth/openid/\"" -i /app/data/config.yml
  yq e ".auth.openid.providers=[]" -i /app/data/config.yml
  yq e ".auth.openid.providers[0].name=\"Cloudron\"" -i /app/data/config.yml
  yq e ".auth.openid.providers[0].authurl=\"${CLOUDRON_OIDC_ISSUER}\"" -i /app/data/config.yml
  yq e ".auth.openid.providers[0].clientid=\"${CLOUDRON_OIDC_CLIENT_ID}\"" -i /app/data/config.yml
  yq e ".auth.openid.providers[0].clientsecret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" -i /app/data/config.yml
else
  yq e ".auth.openid.enabled=false" -i /app/data/config.yml
fi

echo "=> Starting Vikunja"
cd /app/data
exec gosu cloudron:cloudron /usr/bin/vikunja
