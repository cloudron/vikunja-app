#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCAL_USERNAME = 'cloudron';
    const LOCAL_EMAIL = 'admin@cloudron.local';
    const LOCAL_PASSWORD = 'changeme';
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    const LABEL_ID = Math.floor((Math.random() * 100) + 1);
    const LABEL_TITLE = 'Cloudron Label ' + LABEL_ID;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function registerUser() {
        await browser.get(`https://${app.fqdn}/register`);
        await waitForElement(By.id('register-submit'));
        await browser.findElement(By.id('username')).sendKeys(LOCAL_USERNAME);
        await browser.findElement(By.id('email')).sendKeys(LOCAL_EMAIL);
        if (app.manifest.version === '1.0.3') {
            await browser.findElement(By.id('password1')).sendKeys(LOCAL_PASSWORD);
            await browser.findElement(By.id('password2')).sendKeys(LOCAL_PASSWORD);
        } else {
            await browser.findElement(By.id('password')).sendKeys(LOCAL_PASSWORD);
        }
        await browser.findElement(By.id('registerform')).submit();
        await sleep(4000);
        await waitForElement(By.xpath(`//span[contains(text(), "${LOCAL_USERNAME}")]`));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.id('username'));

        await browser.findElement(By.id('username')).sendKeys(LOCAL_USERNAME);
        await browser.findElement(By.id('password')).sendKeys(LOCAL_PASSWORD);
        await browser.findElement(By.id('loginform')).submit();

        await sleep(2000);

        await waitForElement(By.xpath(`//span[contains(text(), "${LOCAL_USERNAME}")]`));
    }

    async function clickOIDCLogin() {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn + '/login');
        await waitForElement(By.xpath('//button[contains(., "Log in with Cloudron")]'));
        await browser.findElement(By.xpath('//button[contains(., "Log in with Cloudron")]')).click();
        await waitForElement(By.xpath('//h3[text()="Current Tasks"]'));
    }


    async function loginOIDC(identifier, password) {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn + '/login');

        await waitForElement(By.xpath('//button[contains(., "Log in with Cloudron")]'));
        await browser.findElement(By.xpath('//button[contains(., "Log in with Cloudron")]')).click();

        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(identifier);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.id('loginSubmitButton')).click();

        await waitForElement(By.xpath('//h3[text()="Current Tasks"]'));
    }

    async function createLabel() {
        await browser.get(`https://${app.fqdn}/labels/new`);

        await waitForElement(By.id('labelTitle'));

        await browser.findElement(By.id('labelTitle')).sendKeys(LABEL_TITLE);
        await browser.findElement(By.id('labelTitle')).sendKeys(Key.ENTER);

        await sleep(3000);
    }

    async function checkLabel() {
        await browser.get(`https://${app.fqdn}/labels`);
        await waitForElement(By.xpath('//button[text()="' + LABEL_TITLE + '"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(@class, "username-dropdown-trigger")]'));
        await browser.findElement(By.xpath('//button[contains(@class, "username-dropdown-trigger")]')).click();

        await waitForElement(By.xpath('//button[./span[text()="Logout"]]'));
        await browser.findElement(By.xpath('//button[./span[text()="Logout"]]')).click();

        await waitForElement(By.xpath('//h2[text()="Login"]'));

        await browser.get('about:blank');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('can install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can register new user', registerUser);
    it('can create label', createLabel);
    it('can logout', logout);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // sso
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can create label', createLabel);
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });

    it('can click OIDC Login', clickOIDCLogin);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can click OIDC Login', clickOIDCLogin);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can click OIDC Login', clickOIDCLogin);
    it('can check label', checkLabel);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app (for update)', function () { execSync(`cloudron install --appstore-id io.vikunja.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login OIDC', clickOIDCLogin);
    it('can create label', createLabel);

    it('can update', async function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login OIDC', clickOIDCLogin);
    it('can check label', checkLabel);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
